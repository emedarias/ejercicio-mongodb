const {config} = require('dotenv');
const { initDatabase } = require("./db");
const {crearPlato, buscarPlato} = require('./model/menu.js');

async function main()
    {
        config();
        const db = await initDatabase();

        const buscar = await buscarPlato(db);
        console.log(buscar);

        //const crear = await crearPlato("hamburguesa", "$400", "re gorda!");
        //console.log(crear);
    }


main();
