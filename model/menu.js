const mongoose = require("mongoose");

const schemaPlato = new mongoose.Schema({plato:String, precio:String, tipo_de_plato:String});

function initPlato()
	{
		const plato = new mongoose.model("plato", schemaPlato);
		return plato;		
	}

async function crearPlato(nombre, precio, tipo)
    {
        const plato = new mongoose.model("plato", schemaPlato);
        const nuevoPlato = new plato ({plato:nombre, precio:precio, tipo_de_plato:tipo});
        const guardado = await nuevoPlato.save();
        return guardado;
    }

async function buscarPlato(db)
    {
        const guardado = await db.models.plato.find();
        return guardado;
    }

module.exports={initPlato,crearPlato, buscarPlato};