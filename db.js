const mongoose = require('mongoose');
const {initPlato} = require('./model/menu.js');

async function initDatabase()
	{
		const {db_schema, db_user, db_pass, db_host, db_name} = process.env;
  		const url = `${db_schema}://${db_user}:${db_pass}@${db_host}/${db_name}?retryWrites=true&w=majority`;

  		const inicializacionDB = new Promise((resolve, reject) => {
	    	mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
	    	const db = mongoose.connection;

	    	db.on('error', (error) => {
		      console.error('Error de conección:', error);
		      reject(error);
		    });
		  
		    db.once('open', function () {
		      console.log('Nos conectamos!');
		      initPlato();
		      resolve(db);
		    });
		  });

		  return inicializacionDB;
	}

module.exports = {initDatabase};